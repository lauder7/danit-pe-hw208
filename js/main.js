// Looking for paragraphs
let deParagraph = document.querySelectorAll('p');
for (let de of deParagraph){
    de.style.background = '#ff0000';
    console.log(`The paragraph found. It's background color changed.`);
}



// Looking for the id
let deOptionsList = document.getElementById('optionsList');
if (deOptionsList) {
    console.log(``);
    console.log(``);
    console.log(`The element with optionsList id is`);
    console.log(deOptionsList);

    console.log(`It's parent is`);
    console.log(deOptionsList.parentElement);

    console.log(`It's child nodes are`);
    for (let dn of deOptionsList.childNodes)
        console.log(`The node with Name ${dn.nodeName} and Type ${dn.nodeType}`);
}



// There are no such elements really
let deTestParagraph;
deTestParagraph = document.querySelector('.testParagraph');
console.log(``);
console.log(``);
console.log(`The first element of the class testParagraph is`);
console.log(deTestParagraph);
if (deTestParagraph) {
    deTestParagraph.innerHTML = 'This is a paragraph';
    console.log(`The element found. It's content changed.`);
}



// Looking for this class instead
let deOptionsListText;
deOptionsListText = document.querySelector('.options-list-text');
console.log(``);
console.log(``);
console.log(`The first element of the class options-list-text is`);
console.log(deOptionsListText);
if (deOptionsListText) {
    deOptionsListText.innerHTML = 'This is a paragraph';
    console.log(`The element found. It's content changed.`);
}



// Looking for children of the first element of the class
let deMainHeader = document.querySelector('.main-header');
console.log(``);
console.log(``);
console.log(`The children of the first element of the class .main-header are:`);
for (let de of deMainHeader.children) {
    console.log(de);
    de.classList.add('nav-item');
    console.log(`The child found. Class nav-item added.`);
}



// There are no such elements really
let deSectionTitleElems = document.querySelectorAll('.section-title');
console.log(``);
console.log(``);
console.log(`The elements of the section-title class are the following:`);
for (let de of deSectionTitleElems) {
    console.log(de);
}



// Looking for elements of this class instead
let sClassName = 'header-title';
let deElemsOfClass = document.querySelectorAll(`.${sClassName}`);
console.log(``);
console.log(``);
console.log(`The elements of the ${sClassName} class are the following:`);
for (let de of deElemsOfClass) {
    console.log(de);
    de.classList.remove(sClassName);
    console.log(`The element found. Class ${sClassName} removed.`);
}